Lab 5: Choose Your Own Particle Adventure

-------------------
Purpose

Learning to create/edit particle systems in Unity/THREE.js

-------------------
Files

-
Lab5

Contains the Unity Project, which is a Kart Racer with edited particle systems and model/environment colors.

-
README.txt

Describes the objective of live and instructions on how to view/access
the given files.

------------------
Instructions

Google Drive Link to video of Lab:
https://drive.google.com/file/d/1QKM4bVZ7ZxShn8Tq66lVP70ezyVCkbWG/view?usp=sharing

------------------
Description of Edits

Changed the Kart Racer model to red and along with that gave the back left and back right wheels a trail for when he drives.
Everytime he drifts (when the user presses space) a new particle will come out of the previously mentioned wheels, to simulate
a cartoon effect of sparks flying out. Colored in the environment and trees and along with that added a starry night skybox.
There's also a trigger when the kart model crosses the checkpoint that will shoot out confetti, and I added in some speed boosts
on the track.
