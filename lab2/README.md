Lab 2: Introduction to Three.js

-------------------
Purpose

Get accustomed to using Three.js by working with uploading/creating 3D models
and modifying them. 

-------------------
Files

-
part1.html

Contains 3 cubes that will rotate and move in the given scene

-
part2.html

Contains 3 different models that are rotated, positioned, and scaled
differently to show a 3D perspective along with effects of lighitng

-
README.txt

Describes the objective of live and instructions on how to view/access
the given files.

------------------
Instructions

Google Drive Link to Part 1 of Lab:
https://drive.google.com/open?id=166EdNzDLon-Ar38wWrPFR-_RveA2GrWV

Screenshot of Three Models loaded into a Three.js scene:
![](images/Lab_2__Intro_to_Three.js_Part_2.jpg)
