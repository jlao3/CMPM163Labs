Justin Lao
jlao3
Spring 2020
Lab 8: Procedural City Generation

----------
DESCRIPTION

Working within Unity's provided scripting function to create procedurally generated
cities and roads. Also used the built-in particle system to make a snowy weather system.

----------
PARTNER QUESTIONS

Added and messaged my partner (John Duncan) on Discord but never got a reply so I couldn't
do the questions for this part of the lab. (Sorry!)

----------
GOOGLE DRIVE LINK

https://drive.google.com/file/d/1f2-2sUGfYzQcMQVzga4CXy4yTkJiAxvv/view?usp=sharing

----------
EXPLANATION

![](lab8/Your_Name.jpg)
This screenshot is taken from the movie Your Name (2016), I got the inspiration to do a
snowy scene because I think the snow creates a certain calming emotion when looking at it
through a movie and I wanted to replicate it. Thus instead of doing realistic buildings, I
went with more of a cartoon pallette.

----------
FILES

-
Lab8

Contains the built version of the scene (Computer doesn't have enough storage to transfer over entire
file assets, which I've been doing on my external hard drive and transfering over to submit).

-
README.txt

Describes purpose of Lab7 along with what files will be submitted.
Gives description on how to run lab.

----------
INSTRUCTIONS

This program is intended to be run using the Unity executable. Double-click the Unity
executable and survey the scene.